# Getting Started with plek-assignment

## Prerequisites

` Node version: v18.14.2`

## Installation

Install the dependencies
`npm install`

Run the application

`npm run start`

The application should start on `http://localhost:3000`

### Testing

To run the test suite

`npm run test`

It will also create the coverage report in the `/coverage` folder

### Linting

To run a lint check
`npm run lint`

To run a lint fix
`npm run lint:fix`

### Prettier

To run a prettier check
`npm run prettier-check`

To run a prettier fix
`npm run prettier-write`

## Solution Guide

#### 1. The output of Question 1 and Question 2 can be seen in the browser console upon starting the application

#### 2. The answer to Question 3 and Question 4 is in the file

`src/utils/ReactContext/ReactContext.txt`

#### 3. The output of Question 5 and Question 6 can directly be seen on the browser.Upon click of each button the relevant component is rendered.

### Contributors

[Rohit kumar](https://github.com/kumar111222rohit)

## License

`MIT License`
